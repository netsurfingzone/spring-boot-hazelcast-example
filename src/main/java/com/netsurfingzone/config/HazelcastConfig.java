package com.netsurfingzone.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

@Configuration
@EnableJpaRepositories(basePackages = "com.netsurfingzone.repository")
@EnableCaching
public class HazelcastConfig {

	@Bean
	public HazelcastInstance hazelcastInstance() {

		Config config = new Config();
		// MapConfig configuration
		MapConfig mapConfig = new MapConfig();
		mapConfig.setName("book");
		mapConfig.setMaxSizeConfig(new MaxSizeConfig(50, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE));
		mapConfig.setEvictionPolicy(EvictionPolicy.LRU);
		mapConfig.setTimeToLiveSeconds(20);

		config.setInstanceName("netsurfingzone_hazelcast");
		config.addMapConfig(mapConfig);
		return Hazelcast.newHazelcastInstance(config);
	}
}
