package com.netsurfingzone.serviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.netsurfingzone.entity.Book;
import com.netsurfingzone.repository.BookRepository;
import com.netsurfingzone.service.BookService;

@Service("bookServiceImpl")
//@CacheConfig(cacheNames = "book")
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository bookRepository;

	@Transactional
	@Override
	public Book saveBook(Book book) {
		book = bookRepository.save(book);
		return book;
	}

	@Transactional
	@Cacheable(cacheNames = "book")
	public Book findById(int bookId) {
		Optional<Book> bookResponse = bookRepository.findById(bookId);
		return bookResponse.get();
	}
}
